<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests\Stream;

use IO\Stream\Stream;
use IO\Stream\StreamFactory;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * Class StreamTest
 * This class tests the functionalities of {@link Stream} and
 * {@link StreamFactory}.
 *
 * @package Tests\Stream
 */
class StreamTest extends TestCase
{
    public function testStreamCreationWithString()
    {
        $factory = new StreamFactory();

        $stream = $factory->createStream('A basic content');

        self::assertTrue($stream->isSeekable());
        self::assertTrue($stream->isWritable());
        self::assertTrue($stream->isReadable());

        self::assertEquals(15, $stream->getSize());

        self::assertEquals('', $stream->getContents());
        self::assertEquals('A basic content', $stream->__toString());

        $stream->rewind();

        self::assertEquals(0, $stream->tell());
        self::assertEquals('A basic content', $stream->getContents());

        self::assertTrue($stream->eof());

        self::assertEquals(1, $stream->getMetadata('seekable'));
    }

    public function testStreamCreationWithFile()
    {
        $factory = new StreamFactory();

        $stream = $factory->createStreamFromFile(
            __DIR__ . '/Resources/sample_file.txt'
        );

        self::assertTrue($stream->isSeekable());
        self::assertFalse($stream->isWritable());
        self::assertTrue($stream->isReadable());

        $data = $stream->read(7);
        self::assertEquals('A basic', $data);

        self::assertEquals(7, $stream->tell());

        $stream->rewind();

        $data = $stream->read();
        self::assertEquals('A basic file', $data);

        $stream->seek(8);
        self::assertEquals('file', $stream->getContents());
    }

    public function testStreamCreationWithResource()
    {
        $factory = new StreamFactory();
        $file = fopen(__DIR__ . '/Resources/empty_file.txt', 'r+');

        $stream = $factory->createStreamFromResource($file);

        self::assertTrue($stream->isSeekable());
        self::assertTrue($stream->isWritable());
        self::assertTrue($stream->isReadable());

        $stream->write('A second basic file.');
        $stream->rewind();

        self::assertEquals('A second basic file.', $stream->getContents());

        $stream->write(' ');
        $stream->write('More complex now.');

        self::assertEquals(
            'A second basic file. More complex now.',
            $stream->__toString()
        );

        self::assertEquals(38, $stream->getSize());

        $stream->truncate();
        self::assertEquals('', $stream->getContents());

        self::assertEquals(0, $stream->getSize());

        $stream->close();
    }

    public function testRaisesExceptionIfStreamIsClosed()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('The stream is closed');

        $factory = new StreamFactory();
        $stream = $factory->createStream('A basic content');

        $stream->close();

        $stream->read();
    }

    public function testsRaisesExceptionIfStreamIsAlreadyClosed()
    {
        $this->expectExceptionMessage(RuntimeException::class);
        $this->expectExceptionMessage('The stream is already closed');

        $factory = new StreamFactory();
        $stream = $factory->createStream('A basic content');

        $stream->close();

        $stream->close(); // Call to StreamInterface::close() twice
    }

    public function testEof()
    {
        $factory = new StreamFactory();
        $stream = $factory->createStream('a = 5');
        $stream->rewind();

        $stream->read(5);
        self::assertFalse($stream->eof()); // Not eof

        $stream->read(1);
        self::assertTrue($stream->eof()); // Eof
    }
}
