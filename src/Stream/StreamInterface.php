<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace IO\Stream;

use InvalidArgumentException;
use JetBrains\PhpStorm\ExpectedValues;
use RuntimeException;

/**
 * Interface StreamInterface
 * This interface describes a data stream.
 * It wraps a PHP stream.
 *
 * @package HTTP\Stream
 * @see https://www.php.net/manual/fr/book.stream.php
 */
interface StreamInterface
{
    /**
     * Closes the stream and any underlying resources.
     *
     * @throws RuntimeException If the stream has already been closed, or if an
     * error occurred while closing the stream.
     */
    public function close();

    /**
     * Returns the size of the stream.
     *
     * @return int|null The stream size in bytes, or null if unknown or if the
     * stream is closed.
     */
    public function getSize(): ?int;

    /**
     * Returns the current position of the file pointer.
     *
     * @return int The current file pointer position.
     * @throws RuntimeException If the stream his closed or if an error
     * occurred while getting the current position of the file pointer.
     */
    public function tell(): int;

    /**
     * Returns whether the stream is at the end.
     *
     * @return bool <code>true</code> if the stream is at the end of if an
     * error occurred, <code>false</code> otherwise.
     */
    public function eof(): bool;

    /**
     * Returns whether the stream is seekable.
     *
     * @return bool <code>true</code> if the stream is seekable,
     * <code>false</code> if it is not or if the stream is closed.
     */
    public function isSeekable(): bool;

    /**
     * Seeks to the specified position in the stream.
     *
     * @see http://www.php.net/manual/en/function.fseek.php
     * @param int $offset The position.
     * @param int $whence [optional] The method calculation.
     * If no method calculation is specified, it will be set to
     * <code>SEEK_SET</code>.
     * @throws RuntimeException If the stream is closed or if an error
     * occurred while seeking the stream.
     */
    public function seek(
        int $offset,
        #[ExpectedValues([
            SEEK_SET,
            SEEK_CUR,
            SEEK_END
        ])] int $whence = SEEK_SET
    );

    /**
     * Seeks to the beginning of the stream.
     *
     * @throws RuntimeException If an error occurred while seeking the stream.
     */
    public function rewind();

    /**
     * Returns whether the stream is writable.
     *
     * @return bool <code>true</code> if the stream is writable,
     * <code>false</code> if it is not or if the stream is closed.
     */
    public function isWritable(): bool;

    /**
     * Writes data to the stream.
     *
     * @param string $data The string that is to be written.
     * @param int|null $length [optional] The data length to be written.
     * If the length is not specified or is null, all the data will be written.
     * @return int The number of bytes written to the stream.
     * @throws RuntimeException If the stream is closed or if an error
     * occurred while writing to the stream.
     */
    public function write(string $data, ?int $length = null): int;

    /**
     * Truncates a stream.
     *
     * @param int|null $size [optional] The size to truncate to.
     * If the size is not specified or is null, all the file will be
     * truncated.
     * @throws RuntimeException If the stream is closed or if an error
     * occurred while truncating the stream.
     */
    public function truncate(?int $size = null);

    /**
     * Returns whether the stream is readable.
     *
     * @return bool <code>true</code> if the stream is readable,
     * <code>false</code> if it is not or if the stream is closed.
     */
    public function isReadable(): bool;

    /**
     * Reads data from the stream.
     * At most $length bytes will be read. Fewer bytes can be read if no more
     * bytes are available.
     *
     * @param ?int $length [optional] The maximal number of bytes to read.
     * If the length is not specified or is null, all the file will be read.
     * @return string Returns the data read from the stream
     * or an empty string if no bytes are available.
     * @throws RuntimeException If the stream is closed or if an error
     * occurred while reading from the stream.
     */
    public function read(?int $length = null): string;

    /**
     * Returns the remaining contents of the stream.
     *
     * @return string The stream remaining contents.
     * @throws RuntimeException If the stream is closed or if an error
     * occurred while getting the contents of the stream.
     */
    public function getContents(): string;

    /**
     * Returns the metadata of the stream.
     *
     * @link http://php.net/manual/en/function.stream-get-meta-data.php
     * @param string|null $key [optional] The specific metadata to return.
     * If the key is not specified or is null, all the metadata will be
     * returned.
     * @return array|string Return an associative array if no
     * specific metadata is provided. Otherwise, return the specific provided
     * metadata value.
     * @throws RuntimeException If the stream is closed.
     * @throws InvalidArgumentException If the specified metadata is invalid.
     */
    public function getMetadata(
        #[ExpectedValues([
            'timed_out',
            'blocked',
            'eof',
            'unread_bytes',
            'stream_type',
            'wrapper_type',
            'wrapper_data',
            'mode',
            'seekable',
            'uri'
        ])] ?string $key = null
    ): array|string;

    /**
     * Returns the contents of the stream.
     *
     * @return string The stream contents, or an empty string if an error
     * occurred.
     */
    public function __toString(): string;
}
