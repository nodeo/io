<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace IO\Stream;

use InvalidArgumentException;
use RuntimeException;

/**
 * Class StreamFactory
 * This class contains methods to create an {@link StreamInterface} instance.
 *
 * @package HTTP\Stream
 * @see StreamInterface
 */
class StreamFactory
{
    /**
     * @var array Allowed open modes
     */
    private const MODES = [
        'r',
        'r+',
        'w',
        'w+',
        'a',
        'a+',
        'x',
        'x+',
        'c',
        'c+',
        'e'
    ];

    /**
     * Creates a new stream with the specified contents.
     * The contents will be wrapped in a temporally resource.
     * By default, as the content is appended to an empty file, the
     * file cursor will be at the end of the stream.
     *
     * @param string $content The stream contents.
     * @return StreamInterface The created {@link StreamInterface} instance
     * with the specified contents.
     * @throws RuntimeException If an error occurred
     * while wrapping the contents in a temporally resource.
     */
    public function createStream(string $content): StreamInterface
    {
        $stream = $this->createStreamFromFile('php://temp', 'r+');
        $stream->write($content);

        return $stream;
    }

    /**
     * Creates a new stream with the contents of the specified file.
     *
     * @param string $filename The file name.
     * @param string $mode [optional] The open mode. If the open mode is not
     * specified, it will be set on <code>'r'</code> (read-only).
     * @return StreamInterface The created {@link StreamInterface} instance
     * with the specified file contents.
     */
    public function createStreamFromFile(
        string $filename,
        string $mode = 'r'
    ): StreamInterface {
        if (!in_array($mode, self::MODES)) {
            throw new InvalidArgumentException('The given mode is invalid');
        }

        $resource = fopen($filename, $mode);

        if ($resource === false) {
            throw new RuntimeException('The file cannot be opened');
        }

        return $this->createStreamFromResource($resource);
    }

    /**
     * Creates a new stream with the content of the specified resource.
     *
     * @param resource $resource The resource to fill the stream with.
     * @return StreamInterface The created {@link StreamInterface} instance
     * with the specified resource contents.
     */
    public function createStreamFromResource($resource): StreamInterface
    {
        return new Stream($resource);
    }
}
