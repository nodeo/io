<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace IO\Stream;

use Exception;
use InvalidArgumentException;
use JetBrains\PhpStorm\ExpectedValues;
use RuntimeException;

/**
 * Class Stream
 * This class is the implementation for {@link StreamInterface}.
 *
 * @package IO\Stream
 * @see StreamInterface
 */
class Stream implements StreamInterface
{
    /**
     * Defines whether a resource is writable and readable
     * depending on its access type.
     *
     * @var array
     */
    private const READABLE_WRITABLE_HASH = [
        'readable' => [
            'r',
            'w+',
            'r+',
            'x+',
            'c+',
            'rb',
            'w+b',
            'r+b',
            'x+b',
            'c+b',
            'rt',
            'w+t',
            'r+t',
            'x+t',
            'c+t',
            'a+'
        ],
        'writable' => [
            'w',
            'w+',
            'rw',
            'r+',
            'x+',
            'c+',
            'wb',
            'w+b',
            'r+b',
            'x+b',
            'c+b',
            'w+t',
            'r+t',
            'x+t',
            'c+t',
            'a',
            'a+'
        ],
    ];

    /**
     * Defines whether the stream is closed.
     *
     * @var bool
     */
    private bool $closed;

    /**
     * The resource underlying to the stream.
     *
     * @var resource
     */
    private $stream;

    /**
     * Defines whether the stream is seekable.
     *
     * @var bool
     */
    private bool $seekable;

    /**
     * Defines whether the stream is writable.
     *
     * @var bool
     */
    private bool $writable;

    /**
     * Defines whether the stream is readable
     *
     * @var bool.
     */
    private bool $readable;

    /**
     * The stream size.
     *
     * @var int|null
     */
    private ?int $size;

    /**
     * Stream constructor.
     *
     * @param resource $resource
     */
    public function __construct($resource)
    {
        if (!is_resource($resource)) {
            throw new InvalidArgumentException(
                'The given resource is not a resource'
            );
        }

        $this->closed = false;

        $this->stream = $resource;

        $metadata = $this->getMetadata();

        $this->seekable = $metadata['seekable'];

        $mode = $metadata['mode'];

        $this->writable = in_array(
            $mode,
            self::READABLE_WRITABLE_HASH['writable']
        );

        $this->readable = in_array(
            $mode,
            self::READABLE_WRITABLE_HASH['readable']
        );

        $this->updateSize();
    }

    /**
     * Stream destructor.
     */
    public function __destruct()
    {
        if (!$this->isClosed()) {
            $this->close();
        }
    }

    /**
     * @inheritDoc
     */
    public function close()
    {
        if ($this->isClosed()) {
            throw new RuntimeException('The stream is already closed');
        }

        if (fclose($this->stream) === false) {
            throw new RuntimeException(
                'An exception occurred while closing the stream'
            );
        }

        $this->detach();

        $this->closed = true;
    }

    /**
     * Detach any underlying resources from the stream.
     */
    private function detach()
    {
        $this->stream = null;

        $this->size = null;
        $this->readable = $this->writable = $this->seekable = false;
    }

    /**
     * @inheritDoc
     */
    public function getSize(): ?int
    {
        if ($this->isClosed()) {
            return null;
        }

        if (is_null($this->size)) {
            // Try to update size
            $this->updateSize();
        }

        return $this->size;
    }

    /**
     * Updates the stream size.
     */
    private function updateSize()
    {
        $stats = fstat($this->stream);

        if ($stats !== false) {
            $this->size = $stats['size'];
        }
    }

    /**
     * @inheritDoc
     */
    public function tell(): int
    {
        $this->assertStreamIsOpen();

        $tell = ftell($this->stream);

        if ($tell === false) {
            throw new RuntimeException(
                'An error occurred while getting the current pointer position'
            );
        }

        return $tell;
    }

    /**
     * @inheritDoc
     */
    public function eof(): bool
    {
        return $this->isClosed() || feof($this->stream);
    }

    /**
     * @inheritDoc
     */
    public function isSeekable(): bool
    {
        return $this->seekable;
    }

    /**
     * @inheritDoc
     */
    public function seek(
        int $offset,
        #[ExpectedValues([
            SEEK_SET,
            SEEK_CUR,
            SEEK_END
        ])] int $whence = SEEK_SET
    ) {
        $this->assertStreamIsOpen();

        if (!$this->isSeekable()) {
            throw new RuntimeException('The stream is not seekable');
        }

        if (fseek($this->stream, $offset, $whence) === -1) {
            throw new RuntimeException(
                'An error occurred while seeking the stream'
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->seek(0);
    }

    /**
     * @inheritDoc
     */
    public function isWritable(): bool
    {
        return $this->writable;
    }

    /**
     * @inheritDoc
     */
    public function write(string $data, ?int $length = null): int
    {
        $this->assertStreamIsOpen();

        if (!$this->isWritable()) {
            throw new RuntimeException('The stream is not writable');
        }

        $bytes = fwrite($this->stream, $data, $length);

        if ($bytes === false) {
            throw new RuntimeException(
                'An error occurred while writing to the stream'
            );
        }

        $this->size = null; // Update size

        return $bytes;
    }

    /**
     * @inheritDoc
     */
    public function truncate(?int $size = null): void
    {
        $this->assertStreamIsOpen();

        if (ftruncate($this->stream, $size) === false) {
            throw new RuntimeException(
                'Error occurred while truncating the stream'
            );
        }

        $this->size = null;
    }

    /**
     * @inheritDoc
     */
    public function isReadable(): bool
    {
        return $this->readable;
    }

    /**
     * @inheritDoc
     */
    public function read(?int $length = null): string
    {
        $this->assertStreamIsOpen();

        if (!$this->isReadable()) {
            throw new RuntimeException('The stream is not readable');
        }

        if (is_null($length)) {
            $length = $this->getSize();
        }

        $data = fread($this->stream, $length);

        if ($data === false) {
            throw new RuntimeException(
                'An error occurred while reading from the stream'
            );
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function getContents(): string
    {
        $this->assertStreamIsOpen();

        $contents = stream_get_contents($this->stream);

        if ($contents === false) {
            throw new RuntimeException(
                'An error occurred while getting the stream contents'
            );
        }

        return $contents;
    }

    /**
     * @inheritDoc
     */
    public function getMetadata(
        #[ExpectedValues([
            'timed_out',
            'blocked',
            'eof',
            'unread_bytes',
            'stream_type',
            'wrapper_type',
            'wrapper_data',
            'mode',
            'seekable',
            'uri',
            null
        ])] string $key = null
    ): array|string {
        $this->assertStreamIsOpen();

        $metadata = stream_get_meta_data($this->stream);

        if ($key === null) {
            return $metadata;
        } else {
            if (!array_key_exists($key, $metadata)) {
                throw new InvalidArgumentException('The given key is invalid');
            }

            return $metadata[$key];
        }
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        try {
            $this->rewind();

            return $this->getContents();
        } catch (Exception) {
            return '';
        }
    }

    /**
     * Asserts that the stream is open.
     *
     * @throws RuntimeException If the stream is closed.
     */
    private function assertStreamIsOpen()
    {
        if ($this->isClosed()) {
            throw new RuntimeException('The stream is closed');
        }
    }

    /**
     * Checks whether the stream is closed.
     *
     * @return bool <code>true</code> if the stream is closed,
     * <code>false</code> otherwise.
     */
    private function isClosed(): bool
    {
        return $this->closed;
    }
}
